package qa.emp.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import qa.emp.Employee;
import qa.emp.EmployeeDAO;
import qa.emp.EmployeeUtils;

@Controller
public class DisplayController {
	@Autowired
	private EmployeeDAO dao;
	@Autowired
	private EmployeeUtils utils;
	
	@RequestMapping("/display")
	public String display(Model model) {
		System.out.println("/display");
		model.addAttribute("dao", dao);
		return "display";
	}
	
	@RequestMapping("/home")
	public String home(Model model) {
		System.out.println("/home");
		model.addAttribute("dao", dao);
		model.addAttribute("utils", utils);
		return "home";
	}
	
	@RequestMapping("/adding")
	public String adding() {
		return "adding";
	}
	
	@RequestMapping("/add")
	public String add(Model model, HttpServletRequest request) {
		String page = "home";
		model.addAttribute("dao", dao);
		model.addAttribute("utils", utils);
		try {
			String idString = request.getParameter("id");
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String ageString = request.getParameter("age");
			String salaryString = request.getParameter("salary");
			String department = request.getParameter("department");
			int age = Integer.parseInt(ageString);
			int id = Integer.parseInt(idString);
			double salary = Double.parseDouble(salaryString);
			Employee emp = new Employee(id, firstname, lastname, age, salary, department);
			if (emp.getAge() < 18) {
				throw new IllegalArgumentException("Age must be 18");
			}
			dao.insertEmployee(emp);			
			page = "added";
		} catch (RuntimeException e) {
			model.addAttribute("message", e.getMessage());
			page = "notadded";
		}
		return page;
	}
	
}
