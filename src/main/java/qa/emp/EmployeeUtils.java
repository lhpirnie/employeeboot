package qa.emp;

import java.util.List;

public class EmployeeUtils {

    private EmployeeDAO dao;

    public EmployeeUtils(EmployeeDAO dao) {
        this.dao = dao;
    }

    public Employee getHighestPaid() {
    	double highestSoFar = 0.0;
    	Employee highestEmployeeSoFar = null;
    	for (Employee e : dao.getAllEmployees()) {
    		if (e.getSalary() >= highestSoFar) {
    			highestSoFar = e.getSalary();
    			highestEmployeeSoFar = e;
    		}
    	}
        return highestEmployeeSoFar;
    }

    public Employee getLowestPaid() {
    	double lowestSoFar = Double.MAX_VALUE;
    	Employee lowestEmployeeSoFar = null;
    	for (Employee e : dao.getAllEmployees()) {
    		if (e.getSalary() <= lowestSoFar) {
    			lowestSoFar = e.getSalary();
    			lowestEmployeeSoFar = e;
    		}
    	}
        return lowestEmployeeSoFar;
    }

    public double getAverageSalary() {
    	double sum = 0.0;
    	List<Employee> emps = dao.getAllEmployees();
    	for (Employee e : emps) {
    		sum += e.getSalary();
    	}
        return sum / emps.size();
    }

}
