<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Added</title>
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
</head>
<body>
<h1>Employee Added</h1>
<p>Employee was added successfully!</p>
<jsp:include page="/WEB-INF/views/links.jsp"></jsp:include>
</body>
</html>