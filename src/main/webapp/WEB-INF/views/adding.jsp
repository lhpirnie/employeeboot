<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Employee</title>
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
</head>
<body>
<h1>Add Employee</h1>

Please fill in this form:

<form id="employeeEntry" action="add">
<br />ID:<input name="id" />
<br />Firstname:<input name="firstname" />
<br />Lastname:<input name="lastname" />
<br />Age:<input name="age" />
<br />Salary:<input name="salary" />
<br />Department:<input name="department" />
<br /><input type="submit" />
</form>

<jsp:include page="/WEB-INF/views/links.jsp"></jsp:include>
</body>
</html>