package qa.emp;

import javax.persistence.*;

@Entity @Table(name="employees")
public class Employee {

    private int id;
    private int age;
    private String firstname;
    private String lastname;
    private double salary;
    private String department;
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public Employee() {}

	public Employee(int id, String firstname, String lastname, int age, double salary, String department) {
        this.id = id;
        this.age = age;
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
        this.department = department;
    }

    public void increaseSalary(double percent) {
        salary += salary * (percent / 100.0);
    }

    @Transient
    public String getFullname() {
        return firstname + " " + lastname;
    }

    public void incAge() {
        age++;
    }

    @Column
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Column
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Column
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Id @Column
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
    	this.id = id;
    }

    @Column
    public String getDepartment() {
        return department;
    }

	public void setDepartment(String department) {
		this.department = department;
	}
    


}

