package qa.emp.repo;

import org.springframework.data.repository.CrudRepository;
import qa.emp.Employee;

public interface EmployeeRepository extends CrudRepository<Employee,Integer> {
	public Employee findById(int id);
	
}
