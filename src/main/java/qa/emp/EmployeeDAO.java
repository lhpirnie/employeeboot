package qa.emp;

import java.util.List;

public interface EmployeeDAO {

	public void login(String username, String password);

	public List<Employee> getAllEmployees();

	public void insertEmployee(Employee employee);

}