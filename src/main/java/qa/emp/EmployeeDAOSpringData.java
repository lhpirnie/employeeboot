package qa.emp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import qa.emp.repo.EmployeeRepository;

public class EmployeeDAOSpringData implements EmployeeDAO {

	@Autowired
	private EmployeeRepository repository;
	
	@Override
	public void login(String username, String password) {
		
	}

	@Override
	public List<Employee> getAllEmployees() {
		List<Employee> employees = new ArrayList<>();
		for (Employee e : repository.findAll()) {
			employees.add(e);
		}
		return employees;
	}

	@Override
	public void insertEmployee(Employee employee) {
		repository.save(employee);
	}

	
	
}
