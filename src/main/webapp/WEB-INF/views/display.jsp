<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="qa.emp.*" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Employees</title>
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
</head>
<body>
<h1>All Employees</h1>
<p>
<table border="1" cellpadding="10">
<c:forEach var="emp" items="${requestScope.dao.allEmployees}">
<tr>
	<td><c:out value="${emp.fullname}" /></td>
	<td><c:out value="${emp.age}" /></td>
	<td><c:out value="${emp.salary}" /></td>
	<td><c:out value="${emp.department}" /></td>
</tr> 
</c:forEach>
</table> 
</p>
<jsp:include page="/WEB-INF/views/links.jsp"></jsp:include>
</body>
</html>