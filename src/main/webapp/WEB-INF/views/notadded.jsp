<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Not Added</title>
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
</head>
<body>
<h1>Employee Not Added</h1>
<p>
Employee was not added. Reason:
<ul><li><c:out value="${requestScope.message}" /></li></ul>
</p>
<jsp:include page="/WEB-INF/views/links.jsp"></jsp:include>
</body>
</html>