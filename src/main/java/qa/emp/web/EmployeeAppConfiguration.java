package qa.emp.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import qa.emp.EmployeeDAO;
import qa.emp.EmployeeDAOImpl;
import qa.emp.EmployeeDAOSpringData;
import qa.emp.EmployeeDAOStub;
import qa.emp.EmployeeUtils;

@EnableJpaRepositories("qa.emp.repo") 
@EntityScan("qa.emp")
@SpringBootApplication(scanBasePackages="qa.emp.web")
public class EmployeeAppConfiguration //extends SpringBootServletInitializer 
{
	private EmployeeDAO dao;
	private EmployeeUtils utils;

	public static void main(String[] args) {
		SpringApplication.run(EmployeeAppConfiguration.class, args);
	}
	
	@Bean
	public EmployeeDAO getDao() {
		return dao;
	}
	
	@Bean
	public EmployeeUtils getUtils() {
		return utils;
	}
	
	public EmployeeAppConfiguration() {
		dao = new EmployeeDAOSpringData();
		utils = new EmployeeUtils(dao);
	}

}
